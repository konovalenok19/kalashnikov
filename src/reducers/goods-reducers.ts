const initialState = {
  isFetching: false,
  isError: false,
  goodsList: null,
}

const goodsReducer = (state = initialState, action) => {
  switch (action.type) {

		case 'FETCH_GOOD_REQUEST':
			return {
				...state,
        isFetching: true,
        isError: false,
			};

		case 'FETCH_GOOD_SUCCESS':
			return {
        ...state,
        isFetching: false,
        goodsList: action.payload,
			};

		case 'FETCH_GOOD_FAILURE':
			return {
        ...state,
        isFetching: false,
        isError: action.payload,
        goodsList: null,
			};

		default:
			return state;
	}

};

export default goodsReducer;