import React from 'react';

import styles from './table.module.scss';


const Table = (props: any) => {
  const { goodsList } = props;

  return (
    <table className={ styles.table }>
      <thead className={ styles.thead }>
        <tr>
          <th>ID</th>
          <th>Название</th>
          <th>Тип</th>
          <th>Цвет</th>
          <th>Размер</th>
          <th>В наличии</th>
          <th>Дата поступления</th>
        </tr>
      </thead>
      <tbody>
        {goodsList && goodsList.map(({
          id,
          name,
          type,
          color,
          size,
          inStock,
          dateReceipt,
        }) => (
          <tr key={id}>
            <td>{id}</td>
            <td>{name}</td>
            <td>{type}</td>
            <td>{color}</td>
            <td>{size}</td>
            <td>{inStock ? "Да" : "Нет"}</td>
            <td>{dateReceipt}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default Table;