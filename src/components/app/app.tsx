import React from 'react';
import logo from './logo.svg';
import styles from './app.module.scss';
import "antd/dist/antd.css";

import TableContainer from '../../containers/table-container';
import Filters from '../filters';

const App: React.FC = () => {
  return (
    <div className={ styles.app }>

      <header className={ styles.header }>
        <img src={logo} className={ styles.logo } alt="logo" />
        <div className={ styles.title }>Тестовое задание Калашников</div>
        <a 
          className={ styles.link }
          href="https://drive.google.com/file/d/1wFrsZZ41AB0IZGZ1IHEVzM2gIYPGphhe/view?usp=sharing"
          target="_blank"
          rel="noopener noreferrer"
        >
          Задание
        </a>
      </header>

      <main className={ styles.main }>
        <div className={ styles.leftside }>
          <TableContainer/>
        </div>

        <div className={ styles.rightside }>
          <Filters/>
        </div>
      </main>

    </div>
  );
}

export default App;