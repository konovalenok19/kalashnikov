import React from 'react';
import { Select, Checkbox, DatePicker } from 'antd';

import styles from './filters.module.scss';

const { Option } = Select;
const { RangePicker } = DatePicker;

const Filters: React.FC = (props: any) => {
  return (
    <div className={ styles.filters }>

      <div className={ styles.row }>
        <div className={ styles.rowName }>Тип</div>
        <div className={ styles.rowControl }>
          <Select defaultValue="lucy" style={{ width: "100%" }}>
            <Option value="jack">Jack</Option>
            <Option value="lucy">Lucy</Option>
            <Option value="disabled" disabled>Disabled</Option>
            <Option value="Yiminghe">yiminghe</Option>
          </Select>
        </div>
      </div>

      <div className={ styles.row }>
        <div className={ styles.rowName }>Цвет</div>
        <div className={ styles.rowControl }>
          <Select defaultValue="lucy" style={{ width: "100%" }}>
            <Option value="jack">Jack</Option>
            <Option value="lucy">Lucy</Option>
            <Option value="disabled" disabled>Disabled</Option>
            <Option value="Yiminghe">yiminghe</Option>
          </Select>
        </div>
      </div>

      <div className={ styles.row }>
        <div className={ styles.rowName }>Размер</div>
        <div className={ styles.rowControl }>
          <Select defaultValue="lucy" style={{ width: "100%" }}>
            <Option value="jack">Jack</Option>
            <Option value="lucy">Lucy</Option>
            <Option value="disabled" disabled>Disabled</Option>
            <Option value="Yiminghe">yiminghe</Option>
          </Select>
        </div>
      </div>

      <div className={ styles.row }>
        <div className={ styles.rowName }>В наличии</div>
        <div className={ styles.rowControl }>
          <Checkbox defaultChecked={false} />
        </div>
      </div>

      <div className={ styles.row }>
        <div className={ styles.rowName }>Дата</div>
        <div className={ styles.rowControl }>
          <RangePicker/>
        </div>
      </div>

    </div>
  );
};

export default Filters;