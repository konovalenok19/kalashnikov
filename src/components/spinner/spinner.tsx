import React from "react";
import colors from "../../utils/colors";
import "./spinner.scss";

const Spinner = (props: any) => {
  const { 
    size = "35px",
    color = "#2684ff",
    message = null,
  } = props;

  return (
    <div className="spinner">
      <div
        className="spinner-base"
        style={{
          width: size,
          height: size,
          border: `.25rem solid ${colors.hexToRgba(color, 0.2)}`,
          borderTopColor: color,
          borderRadius: "50%",
        }}
      />
      <div
        style={{
          color: `${colors.hexToRgba(color, 0.9)}`,
          paddingTop: "5px"
        }}>
        {message}
      </div>
    </div>
  );
};

export default Spinner;