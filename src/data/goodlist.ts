import faker from 'faker';

let id = 1;
let sizes = ['XS', 'L', 'M', 'XL', 'XXL'];
let stocks = [true, false];

function getRandomArbitary (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const createGood = () => ({
  id: id++,
  name: faker.commerce.productName(),
  type: faker.commerce.department(),
  color: faker.commerce.color(),
  size: sizes[getRandomArbitary(0, 2)],
  inStock: stocks[getRandomArbitary(0, 1)],
  dateReceipt: new Date(faker.date.past()).toLocaleDateString(),
});

const goodsList = Array.from({ length: 100 }, createGood);

export {
  goodsList,
};