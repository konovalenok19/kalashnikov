import { createStore } from 'redux';

import goodsReducer from './reducers/goods-reducers';

const store = createStore(
  goodsReducer,
);

export default store;