import { goodsList } from '../data/goodlist';

const fetchGoodsRequest = () => ({
  type: 'FETCH_GOOD_REQUEST',
});

const fetchGoodsSuccess = (goodsList: any) => ({
  type: 'FETCH_GOOD_SUCCESS',
  payload: goodsList,
});

const fetchGoodsFailure = (error: any) => ({
  type: 'FETCH_GOOD_FAILURE',
  payload: error,
});

const fetchGoods = (dispatch: any) => () => {
  dispatch(fetchGoodsRequest());

  let promise = new Promise(resolve => {
    setTimeout(() => resolve(), 1500);
  });

  return promise.then(
    () => dispatch(fetchGoodsSuccess(goodsList)),
    (error) => dispatch(fetchGoodsFailure(error))
  );
};

export {
  fetchGoods,
};