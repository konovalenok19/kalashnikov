import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGoods } from '../actions/goods-actions';
import Table from '../components/table';
import Spinner from '../components/spinner';

interface Props {
  isFetching: boolean;
  isError: boolean;
  goodsList: any[];
  fetchGoods: any;
}

class TableContainer extends Component<Props, {}> {

  componentDidMount() {
    this.props.fetchGoods();
  }

  render() {
    let {
      isFetching,
      isError,
      goodsList,
    } = this.props;

    if (isFetching) {
      return (
        <Spinner 
          color="#09d3ac"
          message="Сейчас все будет, минуточку!"
        />
      );
    }

    if (isError) {
      return (
        <div>Error...</div>
      );
    }

    return (
      <Table
        goodsList={goodsList}
      />
    );
  }
};

const mapStateToProps = state => state;

const mapDispatchToProps = dispatch => ({
  fetchGoods: fetchGoods(dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TableContainer);